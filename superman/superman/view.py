from django.shortcuts import render_to_response
from django.template import RequestContext

def home(request):
	temp_name="index.html"
	ctx=RequestContext(request)
	response=render_to_response(temp_name,ctx)
	return response
